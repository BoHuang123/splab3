#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "basicstats.h"
// CS402 SP-LAB3
// Bo Huang (A20479001), Nov-20-2020
// The program calculates mean, median and sd for a given array. 
// how to compile: gcc -lm -o basicstats basicstats.c


int main(int argcount,char *argv[])
{
    double Mean,Median,Sd;
    Mean=0;Median=0;Sd=0;
    char InputFile[1024];
    FILE *fp;
    float *Array;
    int ArrayInitialSize=20,i=0,ArrayCurrentSize=0;
    Array=(float *)malloc(ArrayInitialSize*sizeof(float));
    ArrayCurrentSize=ArrayInitialSize;


    //Beginning of the module: opening file and reading data

    //scanf("%s",InputFile);
    strcpy(InputFile,argv[1]);

    if (fopen(InputFile, "r")==NULL)
    {
        printf("cannot open %s",InputFile);
        exit(0);
    }
    else
        {
            fp=fopen(InputFile,"r");
        }

    while(!feof(fp))
    {
        fscanf(fp,"%f\n",&Array[i]);

        i++;
        if(i==ArrayCurrentSize)
        {
            float *TmpArray;
            TmpArray=(float*)malloc(ArrayCurrentSize*2*sizeof(float));
            memcpy(TmpArray,Array,i*sizeof(float));
            free(Array);
            Array=TmpArray;
            ArrayCurrentSize=ArrayCurrentSize*2;
        }
    }
    int IdleCapacity=0;
    IdleCapacity=ArrayCurrentSize-i;
    ArrayCurrentSize=i;
    fclose(fp);
    //End of the module: opening file and reading data

    /*for(i=0;i<ArrayCurrentSize;i++)
    {
        printf("Array[%d]=%f\n",i,Array[i]);
    }*/

    Mean=mean_calc(Array,ArrayCurrentSize);
    Sd=sd_calc(Array,ArrayCurrentSize);
    Median=median_calc(Array,ArrayCurrentSize);
    printf("Result:\n--------\nNum Values: %16d\n      mean: %16.3f\n    median: %16.3f\n    stddev: %16.3f\nUnused array capacity: %d\n",ArrayCurrentSize,Mean,Median,Sd,IdleCapacity);






}


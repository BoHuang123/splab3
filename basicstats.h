double sum_calc(float *ArrayContent, int ArraySize)
//calculate the sum of the given array
{
    double ArraySum;
    int i;
    ArraySum=0;
    for (i=0;i< ArraySize;i++)
    {
        ArraySum=ArraySum+ArrayContent[i];
    }
    return(ArraySum);

}
double mean_calc(float *ArrayContent, int ArraySize)
//calculate the mean of the given array
{
    return(sum_calc(ArrayContent,ArraySize)/ArraySize);
}

double sd_calc(float *ArrayContent, int ArraySize)
//calculate the stddev of the given array
{
    int i=0;
    double ArrayMean,tmp=0;
    ArrayMean=mean_calc(ArrayContent,ArraySize);
    for (i=0;i<ArraySize;i++)
    {


        tmp=(ArrayContent[i]-ArrayMean)*(ArrayContent[i]-ArrayMean)+tmp;

    }
    tmp=tmp/ArraySize;
    return(sqrt(tmp));
}
void SortArray(float *ArrayContent, int ArraySize)
//sort the array
{

    int left = 0, right = ArraySize - 1;
    float temp=0;

    while (left<right) {

        for (int i =left; i < right; i++)
        {
            if (ArrayContent[i] > ArrayContent[i + 1])
            {
                 temp = ArrayContent[i]; ArrayContent[i] = ArrayContent[i + 1]; ArrayContent[i + 1] = temp;
            }
        }
        right--;

        for (int j = right;j> left; j--)
        {
            if (ArrayContent[j + 1] < ArrayContent[j])
            {
                 temp = ArrayContent[j]; ArrayContent[j] = ArrayContent[j + 1]; ArrayContent[j + 1] = temp;
            }
        }
        left++;
    }

}


double median_calc(float *ArrayContent, int ArraySize)
//calculate the median of the given array
{
    SortArray(ArrayContent,ArraySize);

    if (ArraySize%2==0)
    {
        return (ArrayContent[ArraySize/2]+ArrayContent[ArraySize/2-1])/2;
    }
    else
    {
        return(ArrayContent[ArraySize/2]);
    }
}



